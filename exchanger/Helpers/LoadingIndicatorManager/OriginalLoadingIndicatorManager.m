//
//  OriginalLoadingIndicatorManager.m
//  exchanger
//
//  Created by Konstantin Mordan on 11/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "OriginalLoadingIndicatorManager.h"
#import "LoadingIndicatorView.h"

@implementation OriginalLoadingIndicatorManager

- (void)showLoadingIndicatorOnView:(UIView *)view {
    LoadingIndicatorView *indicator = [[LoadingIndicatorView alloc] initWithFrame:view.bounds];
    indicator.backgroundColor = [UIColor whiteColor];
    
    [view addSubview:indicator];
}

- (void)hideLoadingIndicatorFromView:(UIView *)view {
    for (UIView *subview in view.subviews) {
        if ([subview isKindOfClass:[LoadingIndicatorView class]]) {
            [subview removeFromSuperview];
            
            break;
        }
    }
}

@end
