//
//  OriginalLoadingIndicatorManager.h
//  exchanger
//
//  Created by Konstantin Mordan on 11/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoadingIndicatorManager.h"

/**
 @author Konstantin Mordan
 
 Объект, который управляет индикатором загрузки
 */
@interface OriginalLoadingIndicatorManager : NSObject <LoadingIndicatorManager>

@end
