//
//  LoadingIndicatorManager.h
//  exchanger
//
//  Created by Konstantin Mordan on 11/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 @author Konstantin Mordan
 
 Протокол объекта, который управляет индикатором загрузки
 */
@protocol LoadingIndicatorManager <NSObject>

/**
 @author Konstantin Mordan
 
 Метод добавляет индикатор загрузки на вью

 @param view Вью, на которую будет добавлен индикатор загрузки
 */
- (void)showLoadingIndicatorOnView:(UIView *)view;

/**
 @author Konstantin Mordan
 
 Метод убирает индикатор загрузки у вью
 
 @param view Вью, у которой необходимо убрать индикатор загрузки
 */
- (void)hideLoadingIndicatorFromView:(UIView *)view;

@end

NS_ASSUME_NONNULL_END
