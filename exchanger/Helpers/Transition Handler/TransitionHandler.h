//
//  TransitionHandler.h
//  exchanger
//
//  Created by Konstantin Mordan on 09/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TransitionHandler <NSObject>

- (void)km_showModule:(UIViewController *)module
             animated:(BOOL)animated;

- (void)km_closeCurrentModuleAnimated:(BOOL)animated;

@end
