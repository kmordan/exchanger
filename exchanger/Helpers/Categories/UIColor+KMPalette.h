//
//  UIColor+KMPalette.h
//  exchanger
//
//  Created by Konstantin Mordan on 23/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (KMPalette)

+ (UIColor *)km_primaryColor;
+ (UIColor *)km_secondaryColor;
+ (UIColor *)km_thirdColor;
+ (UIColor *)km_accentColor;

@end
