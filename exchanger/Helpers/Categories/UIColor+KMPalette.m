//
//  UIColor+KMPalette.m
//  exchanger
//
//  Created by Konstantin Mordan on 23/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "UIColor+KMPalette.h"

@implementation UIColor (KMPalette)

+ (UIColor *)km_primaryColor {
    return [UIColor colorWithRed:33.0/255.0
                           green:33.0/255.0
                            blue:33.0/255.0
                           alpha:1.0];
}

+ (UIColor *)km_secondaryColor {
    return [UIColor colorWithRed:93.0/255.0
                           green:93.0/255.0
                            blue:93.0/255.0
                           alpha:1.0];
    
}

+ (UIColor *)km_thirdColor {
    return [UIColor colorWithRed:189.0/255.0
                           green:189.0/255.0
                            blue:189.0/255.0
                           alpha:1.0];
}

+ (UIColor *)km_accentColor {
    return [UIColor colorWithRed:245.0/255.0
                           green:0.0/255.0
                            blue:87.0/255.0
                           alpha:1.0];
}

@end
