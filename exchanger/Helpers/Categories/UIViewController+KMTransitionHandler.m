//
//  UIViewController+KMTransitionHandler.m
//  exchanger
//
//  Created by Konstantin Mordan on 09/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "UIViewController+KMTransitionHandler.h"

@implementation UIViewController (KMTransitionHandler)

- (void)km_showModule:(UIViewController *)module
             animated:(BOOL)animated {
    [self.navigationController pushViewController:module
                                         animated:animated];
}

- (void)km_closeCurrentModuleAnimated:(BOOL)animated {
    BOOL isInNavigationStack = [self.parentViewController isKindOfClass:[UINavigationController class]];
    BOOL hasManyControllersInStack = isInNavigationStack ? ((UINavigationController *)self.parentViewController).childViewControllers.count > 1u : NO;
    
    if (isInNavigationStack && hasManyControllersInStack) {
        UINavigationController *navigationController = (UINavigationController *)self.parentViewController;
        [navigationController popViewControllerAnimated:animated];
        
    } else {
        [self dismissViewControllerAnimated:animated
                                 completion:nil];
    }
}

@end
