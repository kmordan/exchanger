//
//  UIViewController+KMTransitionHandler.h
//  exchanger
//
//  Created by Konstantin Mordan on 09/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionHandler.h"

@interface UIViewController (KMTransitionHandler) <TransitionHandler>

@end
