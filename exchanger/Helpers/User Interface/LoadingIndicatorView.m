//
//  LoadingIndicatorView.m
//  exchanger
//
//  Created by Konstantin Mordan on 22/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "LoadingIndicatorView.h"

@implementation LoadingIndicatorView

#pragma mark - Initializers

- (instancetype)init {
    self = [super init];
    
    if (self != nil) {
        [self configure];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self != nil) {
        [self configure];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self != nil) {
        [self configure];
    }
    
    return self;
}

#pragma mark - Internal methods

- (void)configure {
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    indicator.color = [UIColor blackColor];
    indicator.center = self.center;
    [indicator startAnimating];
    
    [self addSubview:indicator];
}

@end
