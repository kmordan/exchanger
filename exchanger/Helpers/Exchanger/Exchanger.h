//
//  Exchanger.h
//  exchanger
//
//  Created by Konstantin Mordan on 21/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonConstants.h"

@class ExchangeRatesAttendant;

NS_ASSUME_NONNULL_BEGIN

/**
 @author Konstantin Mordan
 
 Протокол объекта, который выполняет конвертацию из одной валюты в другую
 */
@protocol Exchanger <NSObject>

/**
 @author Konstantin Mordan
 
 Метод выполняет конвертацию

 @param amount                 Сумма, которую необходимо конвертировать
 @param fromCurrency           Валюта из которой необходимо конвертировать
 @param toCurrency             Валюта в которую необходимо конвертировать
 @param exchangeRatesAttendant Объект, обслуживающий курсы валют
 
 @return Реузльтат конвертации
 */
- (NSDecimalNumber *)exchangeAmount:(NSDecimalNumber *)amount
                       fromCurrency:(KMCurrency)fromCurrency
                         toCurrency:(KMCurrency)toCurrency
         withExchangeRatesAttendant:(ExchangeRatesAttendant *)exchangeRatesAttendant;

@end

NS_ASSUME_NONNULL_END
