//
//  OriginalExchanger.h
//  exchanger
//
//  Created by Konstantin Mordan on 21/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Exchanger.h"

/**
 @author Konstantin Mordan
 
 Объект, который выполняет конвертацию из одной валюты в другую
 */
@interface OriginalExchanger : NSObject <Exchanger>

@end
