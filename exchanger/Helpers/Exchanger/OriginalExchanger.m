//
//  OriginalExchanger.m
//  exchanger
//
//  Created by Konstantin Mordan on 21/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "OriginalExchanger.h"
#import "ExchangeRatesAttendant.h"

@implementation OriginalExchanger

- (NSDecimalNumber *)exchangeAmount:(NSDecimalNumber *)amount
                       fromCurrency:(KMCurrency)fromCurrency
                         toCurrency:(KMCurrency)toCurrency
         withExchangeRatesAttendant:(ExchangeRatesAttendant *)exchangeRatesAttendant {
    NSDecimalNumber *rate = [exchangeRatesAttendant exchangeRateFromCurrency:fromCurrency
                                                                  toCurrency:toCurrency];
    
    return [amount decimalNumberByMultiplyingBy:rate];
}

@end
