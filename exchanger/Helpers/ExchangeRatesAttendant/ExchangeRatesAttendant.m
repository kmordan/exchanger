//
//  ExchangeRatesAttendant.m
//  exchanger
//
//  Created by Konstantin Mordan on 21/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "ExchangeRatesAttendant.h"

@interface ExchangeRatesAttendant ()

@property (nonatomic, copy, readwrite) ExchangeRates *exchangeRates;

@end

@implementation ExchangeRatesAttendant

#pragma mark - Initializers

+ (instancetype)attendantWithExchangeRates:(ExchangeRates *)exchangeRates {
    return [[self alloc] initWithExchangeRates:exchangeRates];
}

- (instancetype)initWithExchangeRates:(ExchangeRates *)exchangeRates {
    self = [super init];
    
    if (self != nil) {
        _exchangeRates = [exchangeRates copy];
    }
    
    return self;
}

#pragma mark - Public methods

- (NSDecimalNumber *)exchangeRateFromCurrency:(KMCurrency)fromCurrency
                                   toCurrency:(KMCurrency)toCurrency {
    NSDecimalNumber *fromCurrencyRate = self.exchangeRates[@(fromCurrency)];
    NSDecimalNumber *toCurrencyRate = self.exchangeRates[@(toCurrency)];
    
    NSDecimalNumber *rate = [toCurrencyRate decimalNumberByDividingBy:fromCurrencyRate];
    
    return rate;
}

@end
