//
//  ExchangeRatesAttendant.h
//  exchanger
//
//  Created by Konstantin Mordan on 21/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonConstants.h"

NS_ASSUME_NONNULL_BEGIN

/**
 @author Konstantin Mordan
 
 Объект, обслуживающий курсы валют. Предназначен для рассчета курса валют относительно друг друга
 */
@interface ExchangeRatesAttendant : NSObject

/**
 @author Konstantin Mordan
 
 Курсы валют
 */
@property (nonatomic, copy, readonly) ExchangeRates *exchangeRates;

/**
 @author Konstantin Mordan
 
 Метод рассчитывает курс для передаваемых валют

 @param fromCurrency Валюта из которой выполняется конвертация
 @param toCurrency   Валюта в которую выполняется конвертация
 */
- (NSDecimalNumber *)exchangeRateFromCurrency:(KMCurrency)fromCurrency
                                   toCurrency:(KMCurrency)toCurrency;

+ (instancetype)attendantWithExchangeRates:(ExchangeRates *)exchangeRates;

- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
