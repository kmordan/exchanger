//
//  UserInterfaceConstants.m
//  exchanger
//
//  Created by Konstantin Mordan on 09/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "UserInterfaceConstants.h"

NSString * const KMMainStoryboardName = @"Main";

NSString * const KMTransactionsListViewControllerIdentifier = @"TransactionsListViewController";
NSString * const KMExchangeViewControllerIdentifier = @"ExchangeViewController";
