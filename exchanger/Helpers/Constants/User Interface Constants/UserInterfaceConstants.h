//
//  UserInterfaceConstants.h
//  exchanger
//
//  Created by Konstantin Mordan on 09/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const KMMainStoryboardName;

extern NSString * const KMTransactionsListViewControllerIdentifier;
extern NSString * const KMExchangeViewControllerIdentifier;
