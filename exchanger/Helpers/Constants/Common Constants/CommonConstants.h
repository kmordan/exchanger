//
//  CommonConstants.h
//  exchanger
//
//  Created by Konstantin Mordan on 11/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#ifndef CommonConstants_h
#define CommonConstants_h

typedef NS_ENUM(NSUInteger, KMCurrency) {
    KMCurrencyEUR,
    KMCurrencyUSD,
    KMCurrencyGBP
};

/**
 @author Konstantin Mordan
 
 Тип обмена

 - KMExchangingTypeFrom: Пользователь ввел сумму, которую необходимо обменять
 - KMExchangingTypeTo:   Пользователь ввел сумму, которую неоьходимо получить после обмена
 */
typedef NS_ENUM(NSUInteger, KMExchangingType) {
    KMExchangingTypeFrom,
    KMExchangingTypeTo
};

typedef NSDictionary<NSNumber *, NSDecimalNumber *> ExchangeRates;

#endif /* CommonConstants_h */
