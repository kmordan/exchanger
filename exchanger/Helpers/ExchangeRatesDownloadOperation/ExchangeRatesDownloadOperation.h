//
//  ExchangeRatesDownloadOperation.h
//  exchanger
//
//  Created by Konstantin Mordan on 22/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonConstants.h"

NS_ASSUME_NONNULL_BEGIN

/**
 @author Konstantin Mordan

 Блок с результатом выполнения операции загрузки курсов валют
 
 @param exchangeRates Загруженные курсы валют
 @param error         Ошибка
 */
typedef void (^KMExchangeRatesDownloadOperationCompletionBlock)(ExchangeRates * _Nullable exchangeRates, NSError * _Nullable error);

/**
 @author Konstantin Mordan
 
 Операция загрузки курсов валют
 */
@interface ExchangeRatesDownloadOperation : NSOperation <NSXMLParserDelegate>

+ (instancetype)operationWithCompletionBlock:(KMExchangeRatesDownloadOperationCompletionBlock)completionBlock;

- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
