//
//  ExchangeRatesDownloadOperation.m
//  exchanger
//
//  Created by Konstantin Mordan on 22/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "ExchangeRatesDownloadOperation.h"

static NSString * const KMECBExchangeRatesURLString = @"http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";

static NSString * const KMXMLExchangeRatesCurrencyKey = @"currency";
static NSString * const KMXMLExchangeRatesRateKey = @"rate";

@interface ExchangeRatesDownloadOperation ()

@property (nonatomic, copy) KMExchangeRatesDownloadOperationCompletionBlock operationCompletionBlock;
@property (nonatomic, strong) NSMutableDictionary<NSNumber *, NSDecimalNumber *> *downloadedExchangeRates;

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSURLSessionDataTask *dataTask;

@property (nonatomic, copy) NSArray<NSString *> *requiredCurrencies;
@property (nonatomic, copy) NSDictionary<NSString *, NSNumber *> *currenciesConvertingStrategy;

@property (nonatomic, assign, getter = isExecuting) BOOL executing;
@property (nonatomic, assign, getter = isFinished) BOOL finished;

@end

@implementation ExchangeRatesDownloadOperation

@synthesize executing = _executing;
@synthesize finished = _finished;

#pragma mark - Initializers

+ (instancetype)operationWithCompletionBlock:(KMExchangeRatesDownloadOperationCompletionBlock)completionBlock {
    return [[self alloc] initWithCompletionBlock:completionBlock];
}

- (instancetype)initWithCompletionBlock:(KMExchangeRatesDownloadOperationCompletionBlock)completionBlock {
    self = [super init];
    
    if (self != nil) {
        _operationCompletionBlock = [completionBlock copy];
    }
    
    return self;
}

#pragma mark - NSOperation

- (void)start {
    if (self.isCancelled) {
        self.finished = YES;
        [self reset];
        return;
    }
    
    NSDecimalNumber *eurRate = [NSDecimalNumber decimalNumberWithString:@"1"];
    self.downloadedExchangeRates = [@{ @(KMCurrencyEUR) : eurRate } mutableCopy];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.session = [NSURLSession sessionWithConfiguration:configuration];
    
    NSURL *url = [NSURL URLWithString:KMECBExchangeRatesURLString];
    __weak typeof(self) wSelf = self;
    self.dataTask = [self.session dataTaskWithURL:url
                                completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
    {
        typeof(self) sSelf = wSelf;
        
        if (error != nil) {
            [sSelf completeOperationWithDownloadedExchangeRates:nil
                                                          error:error];
            
        } else if (data != nil) {
            NSXMLParser *myParser = [[NSXMLParser alloc] initWithData:data];
            [myParser setDelegate:sSelf];
            [myParser parse];
            
        } else {
            [sSelf completeOperationWithDownloadedExchangeRates:nil
                                                          error:nil];
        }
    }];
    
    self.executing = YES;
    
    [self.dataTask resume];
}

- (void)cancel {
    [super cancel];
    
    self.executing = NO;
    self.finished = YES;
    
    [self reset];
}

- (void)setFinished:(BOOL)finished {
    [self willChangeValueForKey:NSStringFromSelector(@selector(isFinished))];
    _finished = finished;
    [self didChangeValueForKey:NSStringFromSelector(@selector(isFinished))];
}

- (void)setExecuting:(BOOL)executing {
    [self willChangeValueForKey:NSStringFromSelector(@selector(isExecuting))];
    _executing = executing;
    [self didChangeValueForKey:NSStringFromSelector(@selector(isExecuting))];
}

#pragma mark - NSXMLParserDelegate

- (void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary<NSString *, NSString *> *)attributeDict {
    if (self.isCancelled) {
        self.finished = YES;
        [self reset];
        return;
    }
    
    NSString *currency = attributeDict[KMXMLExchangeRatesCurrencyKey];
    
    if (currency != nil && [self.requiredCurrencies containsObject:currency]) {
        NSString *rateString = attributeDict[KMXMLExchangeRatesRateKey];
        
        NSDecimalNumber *rate = [NSDecimalNumber decimalNumberWithString:rateString];
        NSNumber *kmCurrency = self.currenciesConvertingStrategy[currency];
        
        self.downloadedExchangeRates[kmCurrency] = rate;
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    [self completeOperationWithDownloadedExchangeRates:[self.downloadedExchangeRates copy]
                                                 error:nil];
}

#pragma mark - Internal methods

- (void)reset {
    self.operationCompletionBlock = nil;
    self.downloadedExchangeRates = nil;
    
    [self.session invalidateAndCancel];
    self.session = nil;
    
    [self.dataTask cancel];
    self.dataTask = nil;
}

- (void)done {
    self.finished = YES;
    self.executing = NO;
    [self reset];
}

- (void)completeOperationWithDownloadedExchangeRates:(ExchangeRates *)downloadedExchangeRates
                                               error:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.operationCompletionBlock != nil) {
            self.operationCompletionBlock(downloadedExchangeRates, error);
        }
        
        [self done];
    });
}

- (NSArray<NSString *> *)requiredCurrencies {
    if (_requiredCurrencies == nil) {
        _requiredCurrencies = @[@"EUR", @"USD", @"GBP"];
    }
    
    return _requiredCurrencies;
}

- (NSDictionary<NSString *, NSNumber *> *)currenciesConvertingStrategy {
    if (_currenciesConvertingStrategy == nil) {
        _currenciesConvertingStrategy = @{ @"EUR" : @(KMCurrencyEUR),
                                           @"USD" : @(KMCurrencyUSD),
                                           @"GBP" : @(KMCurrencyGBP) };
    }
    
    return _currenciesConvertingStrategy;
}

@end
