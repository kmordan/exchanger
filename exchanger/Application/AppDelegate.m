//
//  AppDelegate.m
//  exchanger
//
//  Created by Konstantin Mordan on 03/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "AppDelegate.h"
#import "ExchangeAssemblyImplementation.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    id<ExchangeAssembly> exchangeAssembly = [ExchangeAssemblyImplementation new];
    
    UIViewController *exchangeVC = [exchangeAssembly exchangeViewController];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:exchangeVC];
    self.window.rootViewController = nc;
    
    [self.window makeKeyAndVisible];

    return YES;
}

@end
