//
//  AccountsStorageImplementation.h
//  exchanger
//
//  Created by Konstantin Mordan on 22/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountsStorage.h"

/**
 @author Konstantin Mordan
 
 Объект, отвечающий за хранение счетов
 */
@interface AccountsStorageImplementation : NSObject <AccountsStorage>

@end
