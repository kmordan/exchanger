//
//  AccountsStorageImplementation.m
//  exchanger
//
//  Created by Konstantin Mordan on 22/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "AccountsStorageImplementation.h"
#import "Account.h"

static NSString * const KMAccountInitialBalanceAmountString = @"100.0";

@interface AccountsStorageImplementation ()

@property (nonatomic, copy) NSArray<Account *> *accounts;

@end

@implementation AccountsStorageImplementation

#pragma mark - AccountStorage

- (void)saveAccounts:(NSArray<Account *> *)accounts
       withComletion:(KMAccountStorageCompletionBlock)completion {
    self.accounts = accounts;
    
    if (completion != nil) {
        completion(self.accounts, YES);
    }
}

- (NSArray<Account *> *)obtainAccounts {
    return self.accounts;
}

#pragma mark - Internal methods

- (NSArray<Account *> *)accounts {
    if (_accounts == nil) {
        NSDecimalNumber *initialAmount = [NSDecimalNumber decimalNumberWithString:KMAccountInitialBalanceAmountString];
        
        Account *eur = [Account accountWithBalance:initialAmount
                                      ofCurrency:KMCurrencyEUR];
        Account *gbp = [Account accountWithBalance:initialAmount
                                      ofCurrency:KMCurrencyGBP];
        Account *usd = [Account accountWithBalance:initialAmount
                                      ofCurrency:KMCurrencyUSD];
        
        _accounts = @[eur, gbp, usd];
    }
    
    return _accounts;
}

@end
