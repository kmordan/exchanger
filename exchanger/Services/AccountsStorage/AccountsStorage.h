//
//  AccountStorage.h
//  exchanger
//
//  Created by Konstantin Mordan on 22/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Account;

NS_ASSUME_NONNULL_BEGIN

/**
 @author Konstantin Mordan
 
 Блок с результатом работы сервиса хранения счетов

 @param savedAccounts Массив счетов, которые были сохранены
 @param success       Флаг, который говорит о том, получилось ли сохранить счета
 */
typedef void (^KMAccountStorageCompletionBlock)(NSArray<Account *> *savedAccounts, BOOL success);

/**
 @author Konstantin Mordan
 
 Протокол объекта, отвечающего за хранение счетов
 */
@protocol AccountsStorage <NSObject>

/**
 @author Konstantin Mordan
 
 Метод выполняет сохранние счетов

 @param accounts   Массив счетов, которые необходимо сохранить
 @param completion Блок с результатом выполнения
 */
- (void)saveAccounts:(NSArray<Account *> *)accounts
       withComletion:(KMAccountStorageCompletionBlock)completion;

/**
 @author Konstantin Mordan
 
 Метод для получения счетов
 */
- (NSArray<Account *> *)obtainAccounts;

@end

NS_ASSUME_NONNULL_END
