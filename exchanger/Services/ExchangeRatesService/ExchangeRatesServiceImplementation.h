//
//  ExchangeRatesServiceImplementation.h
//  exchanger
//
//  Created by Konstantin Mordan on 22/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ExchangeRatesService.h"

NS_ASSUME_NONNULL_BEGIN

/**
 @author Konstantin Mordan
 
 Объект, отвечающий за получение курсов валют
 */
@interface ExchangeRatesServiceImplementation : NSObject <ExchangeRatesService>

@property (nonatomic, strong) NSOperationQueue *queue;

@end

NS_ASSUME_NONNULL_END
