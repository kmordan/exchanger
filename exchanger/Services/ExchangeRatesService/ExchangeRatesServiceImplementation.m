//
//  ExchangeRatesServiceImplementation.m
//  exchanger
//
//  Created by Konstantin Mordan on 22/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "ExchangeRatesServiceImplementation.h"
#import "ExchangeRatesDownloadOperation.h"

@implementation ExchangeRatesServiceImplementation

#pragma mark - Lifecycle

- (void)dealloc {
    [self.queue cancelAllOperations];
}

#pragma mark - Public methods

- (void)obtainExchangeRatesWithCompletion:(KMExchangeRatesServiceCompletionBlock)completion {
    ExchangeRatesDownloadOperation *operation = [ExchangeRatesDownloadOperation operationWithCompletionBlock:completion];
    
    [self.queue addOperation:operation];
}

- (void)cancelObtainingExchangeRates {
    [self.queue cancelAllOperations];
}

@end
