//
//  ExchangeRatesService.h
//  exchanger
//
//  Created by Konstantin Mordan on 22/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonConstants.h"

/**
 @author Konstantin Mordan
 
 Блок с результатом выполнения сервиса получения курсов валют

 @param exchangeRates Полученные курсы валют
 @param error         Ошибка
 */
typedef void (^KMExchangeRatesServiceCompletionBlock)(ExchangeRates * _Nullable exchangeRates, NSError * _Nullable error);

/**
 @author Konstantin Mordan
 
 Протокол объекта, который отвечает за получение курсов валют
 */
@protocol ExchangeRatesService <NSObject>

/**
 @author Konstantin Mordan
 
 Метод инициирует получение курсов валют

 @param completion Блок, который необходимо выполнить после получения курсов валют
 */
- (void)obtainExchangeRatesWithCompletion:(KMExchangeRatesServiceCompletionBlock _Nullable)completion;

/**
 @author Konstantin Mordan
 
 Метод инициирует отмену получения курсов валют
 */
- (void)cancelObtainingExchangeRates;

@end
