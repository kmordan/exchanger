//
//  ExchangeViewInput.h
//  exchanger
//
//  Created by Konstantin Mordan on 09/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ExchangeViewModel;

NS_ASSUME_NONNULL_BEGIN

@protocol ExchangeViewInput <NSObject>

/**
 @author Konstantin Mordan
 
 Метод, инициирующий отображение индикатора загрузки
 */
- (void)showLoadingIndicator;

/**
 @author Konstantin Mordan
 
 Метод, инициирующий скрытие индикатора загрузки
 */
- (void)hideLoadingIndicator;

/**
 @author Konstantin Mordan
 
 Метод, инициирующий обновление вью

 @param viewModel Вью-модуль, с которой необходимо обновить вью
 */
- (void)updateWithViewModel:(ExchangeViewModel *)viewModel;

@end

NS_ASSUME_NONNULL_END
