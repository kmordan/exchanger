//
//  ExchangeViewFootprint.m
//  exchanger
//
//  Created by Konstantin Mordan on 19/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "ExchangeViewFootprint.h"

@interface ExchangeViewFootprint ()

@property (nonatomic, assign, readwrite) KMExchangingType exchangingType;

@property (nonatomic, strong, readwrite) NSDecimalNumber *amount;
@property (nonatomic, assign, readwrite) KMCurrency fromCurrency;
@property (nonatomic, assign, readwrite) KMCurrency toCurrency;

@end

@implementation ExchangeViewFootprint

+ (instancetype)footprintWithExchangingType:(KMExchangingType)exchangingType
                                     amount:(NSDecimalNumber *)amount
                               fromCurrency:(KMCurrency)fromCurrency
                                 toCurrency:(KMCurrency)toCurrency {
    return [[self alloc] initWithExchangingType:exchangingType
                                         amount:amount
                                   fromCurrency:fromCurrency
                                     toCurrency:toCurrency];
}

- (instancetype)initWithExchangingType:(KMExchangingType)exchangingType
                                amount:(NSDecimalNumber *)amount
                          fromCurrency:(KMCurrency)fromCurrency
                            toCurrency:(KMCurrency)toCurrency {
    self = [super init];
    
    if (self != nil) {
        _exchangingType = exchangingType;
        _amount = amount;
        _fromCurrency = fromCurrency;
        _toCurrency = toCurrency;
    }
    
    return self;
}

@end
