//
//  ExchangeViewController.m
//  exchanger
//
//  Created by Konstantin Mordan on 09/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "ExchangeViewController.h"
#import "ExchangeViewOutput.h"
#import "LoadingIndicatorManager.h"
#import "AccountSelectionView.h"
#import "ExchangeViewFootprint.h"
#import "ExchangeViewModel.h"

static NSString * const KMExchangeBarButtonItemTitle = @"Exchange";

@interface ExchangeViewController ()

@property (nonatomic, weak) IBOutlet AccountSelectionView *accountSelectionViewForExchangingFrom;
@property (nonatomic, weak) IBOutlet AccountSelectionView *accountSelectionViewForExchangingTo;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bottomLayoutConstraint;

@property (nonatomic, strong) UIBarButtonItem *exchangeBarButtonItem;

@end

@implementation ExchangeViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureNavigationBar];
    
    [self.notificationCenter addObserver:self
                                selector:@selector(keyboardWillChange:)
                                    name:UIKeyboardWillChangeFrameNotification
                                  object:nil];
    
    self.exchangeBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:KMExchangeBarButtonItemTitle
                                                                  style:UIBarButtonItemStyleDone
                                                                 target:self
                                                                 action:@selector(exchangeAction)];
    self.exchangeBarButtonItem.enabled = NO;
    self.navigationItem.rightBarButtonItem = self.exchangeBarButtonItem;
    
    self.accountSelectionViewForExchangingFrom.delegate = self;
    self.accountSelectionViewForExchangingTo.delegate = self;
    
    [self.output didLoadView];
}

- (void)dealloc {
    [self.notificationCenter removeObserver:self];
}

#pragma mark - Actions

- (void)exchangeAction {
    self.exchangeBarButtonItem.enabled = NO;
    
    [self.output didInitiateExchangeWithFootprint:[self generateViewFootprintForExchanging:KMExchangingTypeFrom]];
 }

#pragma mark - ExchangeViewInput

- (void)showLoadingIndicator {
    [self.loadingIndicatorManager showLoadingIndicatorOnView:self.view];
}

- (void)hideLoadingIndicator {
    [self.loadingIndicatorManager hideLoadingIndicatorFromView:self.view];
}

- (void)updateWithViewModel:(ExchangeViewModel *)viewModel {
    [self.accountSelectionViewForExchangingFrom updateWithViewModel:viewModel.accountSelectionViewModelForExchangingFrom];
    [self.accountSelectionViewForExchangingTo updateWithViewModel:viewModel.accountSelectionViewModelForExchangingTo];
    
    self.navigationItem.title = viewModel.rate;
    self.navigationController.navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName: viewModel.titleColor };
    
    self.exchangeBarButtonItem.enabled = viewModel.isExchangeAvailable;
    self.exchangeBarButtonItem.tintColor = viewModel.exchangeButtonColor;
}

#pragma mark - AccountSelectionViewDelegate

- (void)didEnterAmountToAccountSelectionView:(AccountSelectionView *)accountSelectionView {
    ExchangeViewFootprint *footprint = nil;
    
    if ([accountSelectionView isEqual:self.accountSelectionViewForExchangingFrom]) {
        footprint = [self generateViewFootprintForExchanging:KMExchangingTypeFrom];
    }
    
    if ([accountSelectionView isEqual:self.accountSelectionViewForExchangingTo]) {
        footprint = [self generateViewFootprintForExchanging:KMExchangingTypeTo];
    }
    
    [self.output didUpdateFootprint:footprint];
}

- (void)didSelectCurrencyInAccountSelectionView:(AccountSelectionView *)accountSelectionView {
    
    ExchangeViewFootprint *footprint = nil;
    
    if ([self.accountSelectionViewForExchangingFrom doesReceiveInputs]) {
        footprint = [self generateViewFootprintForExchanging:KMExchangingTypeFrom];
    }
    
    if ([self.accountSelectionViewForExchangingTo doesReceiveInputs]) {
        footprint = [self generateViewFootprintForExchanging:KMExchangingTypeTo];
    }
    
    [self.output didUpdateFootprint:footprint];
}

#pragma mark - Helper methods

- (void)keyboardWillChange:(NSNotification *)notification {
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    self.bottomLayoutConstraint.constant = CGRectGetHeight(keyboardRect);
}

- (ExchangeViewFootprint *)generateViewFootprintForExchanging:(KMExchangingType)exchangingType {
    ExchangeViewFootprint *footprint = nil;

    if (exchangingType == KMExchangingTypeFrom) {
        footprint = [ExchangeViewFootprint footprintWithExchangingType:KMExchangingTypeFrom
                                                                amount:self.accountSelectionViewForExchangingFrom.inputtedAmount
                                                          fromCurrency:self.accountSelectionViewForExchangingFrom.selectedCurrency
                                                            toCurrency:self.accountSelectionViewForExchangingTo.selectedCurrency];
    }
    
    if (exchangingType == KMExchangingTypeTo) {
        footprint = [ExchangeViewFootprint footprintWithExchangingType:KMExchangingTypeTo
                                                                amount:self.accountSelectionViewForExchangingTo.inputtedAmount
                                                          fromCurrency:self.accountSelectionViewForExchangingFrom.selectedCurrency
                                                            toCurrency:self.accountSelectionViewForExchangingTo.selectedCurrency];
    }
    
    return footprint;
}

#pragma mark - Helper methods

- (void)configureNavigationBar {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
}

@end
