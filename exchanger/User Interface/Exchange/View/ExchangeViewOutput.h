//
//  ExchangeViewOutput.h
//  exchanger
//
//  Created by Konstantin Mordan on 09/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ExchangeViewFootprint;

@protocol ExchangeViewOutput <NSObject>

/**
 @author Konstantin Mordan
 
 Метод сообщает презентеру о том, что вью была загружена
 */
- (void)didLoadView;

/**
 @author Konstantin Mordan
 
 Метод сообщает презентеру о том, что вью изменилась

 @param newFootprint Отпечаток вьюхи после изменения
 */
- (void)didUpdateFootprint:(ExchangeViewFootprint *)newFootprint;

/**
 @author Konstantin Mordan
 
 Метод сообщает презентеру о том, что была инициирована конвертация
 
 @param footprint Отпечаток вьюхи
 */
- (void)didInitiateExchangeWithFootprint:(ExchangeViewFootprint *)footprint;

@end
