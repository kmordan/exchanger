//
//  AccountSelectionCollectionViewManager.m
//  exchanger
//
//  Created by Konstantin Mordan on 17/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "AccountSelectionCollectionViewManager.h"
#import "AccountCollectionViewCell.h"
#import "tgmath.h"
#import "AccountSelectionCollectionViewManagerDelegate.h"
#import "AccountViewModel.h"

static NSString * const KMCurrencyCollectionViewCellIdentifier = @"KMCurrencyCollectionViewCellIdentifier";
static NSUInteger const KMCurrencyCollectionViewSectionIndex = 0u;

@interface AccountSelectionCollectionViewManager ()

@property (nonatomic, weak, readwrite) UICollectionView *collectionView;
@property (nonatomic, weak, readwrite) id<AccountSelectionCollectionViewManagerDelegate> delegate;

@property (nonatomic, copy) NSArray<AccountViewModel *> *originalAccountViewModels;
@property (nonatomic, copy) NSArray<AccountViewModel *> *loopedAccountViewModels;

@property (nonatomic, strong) AccountViewModel *selectedAccountViewModel;

@end

@implementation AccountSelectionCollectionViewManager

#pragma mark - Initializers

+ (instancetype)managerForCollectionView:(UICollectionView *)collectionView
                            withDelegate:(id<AccountSelectionCollectionViewManagerDelegate>)delegate {
    return [[self alloc] initWithCollectionView:collectionView
                                       delegate:delegate];
}

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView
                              delegate:(id<AccountSelectionCollectionViewManagerDelegate>)delegate {
    self = [super init];
    
    if (self != nil) {
        _collectionView = collectionView;
        _delegate = delegate;
        
        [self configure];
    }
    
    return self;
}

#pragma mark - Public methods

- (void)updateWithViewModels:(NSArray<AccountViewModel *> *)viewModels
    selectedAccountViewModel:(AccountViewModel *)selectedAccountViewModel {
    NSMutableArray *loopedAccountViewModels = [NSMutableArray new];
    [loopedAccountViewModels addObject:viewModels.lastObject];
    [loopedAccountViewModels addObjectsFromArray:viewModels];
    [loopedAccountViewModels addObject:viewModels.firstObject];
    
    self.originalAccountViewModels = viewModels;
    self.loopedAccountViewModels = [loopedAccountViewModels copy];
    self.selectedAccountViewModel = selectedAccountViewModel;

    NSMutableArray<NSIndexPath *> *indexPathes = [NSMutableArray new];
    [self.loopedAccountViewModels enumerateObjectsUsingBlock:^(AccountViewModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {

        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:idx
                                                     inSection:KMCurrencyCollectionViewSectionIndex];
        [indexPathes addObject:indexPath];
    }];
    
    [self.collectionView performBatchUpdates:^{
        [self.collectionView reloadItemsAtIndexPaths:[indexPathes copy]];
    } completion:nil];

    [self showSelectedAccount];
    
    if (selectedAccountViewModel.isFirstResponder) {
        UICollectionViewCell *cell = [self.collectionView visibleCells].firstObject;
        [cell becomeFirstResponder];
    }
}

- (BOOL)doesReceiveInputs {
    NSIndexPath *indexPath = [self.collectionView indexPathsForVisibleItems].firstObject;
    AccountViewModel *viewModel = self.loopedAccountViewModels[indexPath.row];
    
    return viewModel.isFirstResponder;
}

- (void)showSelectedAccount {
    NSInteger index = [self.originalAccountViewModels indexOfObject:self.selectedAccountViewModel];
    
    if (index != NSNotFound) {
        NSInteger indexInLoopedAccountViewModels = index + 1;
        [self showAccountViewModelAtIndex:indexInLoopedAccountViewModels];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return self.loopedAccountViewModels.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:KMCurrencyCollectionViewCellIdentifier
                                                                           forIndexPath:indexPath];
    
    if ([cell isKindOfClass:[AccountCollectionViewCell class]]) {
        AccountViewModel *viewModel = self.loopedAccountViewModels[indexPath.row];
        
        [(AccountCollectionViewCell *)cell updateCellWithViewModel:viewModel
                                                          delegate:self];
    }
    
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat flowOffset = scrollView.contentOffset.x;
    CGFloat itemWidth = CGRectGetWidth(self.collectionView.bounds);
    
    NSInteger currentAccountIndex = ((NSInteger)round(flowOffset / itemWidth));
    
    if ([self shouldResetCurrentAccountIndexForCreatingCircularScrollingEffect:currentAccountIndex]) {
        NSInteger newIndex = [self resetedCurrentAccountIndexForCreatingCircularScrollingEffect:currentAccountIndex];
        
        [self showAccountViewModelAtIndex:newIndex];
    }
    
    AccountViewModel *viewModel = self.loopedAccountViewModels[currentAccountIndex];
    [self.delegate didSelectCurrency:viewModel.currency];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(CGRectGetWidth(self.collectionView.bounds),
                      CGRectGetHeight(self.collectionView.bounds));
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout*)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeZero;
}

#pragma mark - AccountCollectionViewCellDelegate

- (void)didEnterAmount:(NSDecimalNumber *)amount {
    [self.delegate didEnterAmount:amount];
}

#pragma mark - Helper methods

- (void)configure {
    NSString *nibName = NSStringFromClass([AccountCollectionViewCell class]);
    [self.collectionView registerNib:[UINib nibWithNibName:nibName
                                                    bundle:[NSBundle bundleForClass:[self class]]]
          forCellWithReuseIdentifier:KMCurrencyCollectionViewCellIdentifier];
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
}

- (BOOL)shouldResetCurrentAccountIndexForCreatingCircularScrollingEffect:(NSInteger)currentIndex {
    return (currentIndex > 0 && currentIndex <= self.originalAccountViewModels.count) == NO;
}

- (NSInteger)resetedCurrentAccountIndexForCreatingCircularScrollingEffect:(NSInteger)index {
    NSInteger newIndex = NSNotFound;
    
    if (index == 0) {
        newIndex = self.originalAccountViewModels.count;
        
    } else if (index == self.originalAccountViewModels.count + 1) {
        newIndex = 1;
    }
    
    return newIndex;
}

- (void)showAccountViewModelAtIndex:(NSInteger)index {
    if (index < self.loopedAccountViewModels.count) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index
                                                     inSection:KMCurrencyCollectionViewSectionIndex];
        
        [self.collectionView scrollToItemAtIndexPath:indexPath
                                    atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                            animated:NO];
    }
}

@end
