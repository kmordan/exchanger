//
//  AccountSelectionCollectionViewManager.h
//  exchanger
//
//  Created by Konstantin Mordan on 17/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AccountCollectionViewCellDelegate.h"

@class AccountViewModel;

@protocol AccountSelectionCollectionViewManagerDelegate;

NS_ASSUME_NONNULL_BEGIN

@interface AccountSelectionCollectionViewManager : NSObject <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, AccountCollectionViewCellDelegate>

@property (nonatomic, weak, readonly) UICollectionView *collectionView;
@property (nonatomic, weak, readonly) id<AccountSelectionCollectionViewManagerDelegate> delegate;

- (void)updateWithViewModels:(NSArray<AccountViewModel *> *)viewModels
    selectedAccountViewModel:(AccountViewModel *)selectedViewModel;

- (void)showSelectedAccount;

- (BOOL)doesReceiveInputs;

+ (instancetype)managerForCollectionView:(UICollectionView *)collectionView
                            withDelegate:(id<AccountSelectionCollectionViewManagerDelegate>)delegate;

- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
