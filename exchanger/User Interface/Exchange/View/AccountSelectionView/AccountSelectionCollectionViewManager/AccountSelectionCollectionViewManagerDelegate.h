//
//  AccountSelectionCollectionViewManagerDelegate.h
//  exchanger
//
//  Created by Konstantin Mordan on 19/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonConstants.h"

NS_ASSUME_NONNULL_BEGIN

@protocol AccountSelectionCollectionViewManagerDelegate <NSObject>

- (void)didSelectCurrency:(KMCurrency)currency;
- (void)didEnterAmount:(NSDecimalNumber *)amount;

@end

NS_ASSUME_NONNULL_END
