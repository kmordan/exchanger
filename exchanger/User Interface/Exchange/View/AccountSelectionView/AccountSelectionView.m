//
//  AccountSelectionView.m
//  exchanger
//
//  Created by Konstantin Mordan on 12/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "AccountSelectionView.h"
#import "AccountSelectionCollectionViewManager.h"
#import "AccountSelectionViewDelegate.h"
#import "AccountSelectionViewModel.h"

static CGFloat const KMPageControlHeight = 40.0;
static CGFloat const KMPageControlWidth = 100.0;
static CGFloat const KMPageControlBottomInset = 10.0;

@interface AccountSelectionView ()

@property (nonatomic, assign, readwrite) KMCurrency selectedCurrency;
@property (nonatomic, assign, readwrite) NSDecimalNumber *inputtedAmount;

@end

@implementation AccountSelectionView

#pragma mark - Lifecycle

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UICollectionViewFlowLayout *viewLayout = [[UICollectionViewFlowLayout alloc] init];
    viewLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:[self collectionViewFrame]
                                             collectionViewLayout:viewLayout];

    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.pagingEnabled = YES;
    
    [self addSubview:self.collectionView];
    
    self.collectionViewManager = [AccountSelectionCollectionViewManager managerForCollectionView:self.collectionView
                                                                                    withDelegate:self];
    
    self.pageControl = [[UIPageControl alloc] initWithFrame:[self pageControlFrame]];
    self.pageControl.userInteractionEnabled = NO;
    [self addSubview:self.pageControl];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.collectionView.collectionViewLayout invalidateLayout];
    self.collectionView.frame = [self collectionViewFrame];
    [self.collectionView.collectionViewLayout prepareLayout];
    
    [self.collectionViewManager showSelectedAccount];
    
    self.pageControl.frame = [self pageControlFrame];
}

- (CGRect)collectionViewFrame {
    return CGRectMake(0.0,
                      0.0,
                      CGRectGetWidth(self.bounds),
                      CGRectGetHeight(self.bounds));
}

- (CGRect)pageControlFrame {
    CGFloat x = CGRectGetMidX(self.bounds) - (KMPageControlWidth / 2.0);
    CGFloat y = CGRectGetMaxY(self.bounds) - KMPageControlHeight - KMPageControlBottomInset;
    
    return CGRectMake(x,
                      y,
                      KMPageControlWidth,
                      KMPageControlHeight);
}

#pragma mark - Public methods

- (void)updateWithViewModel:(AccountSelectionViewModel *)viewModel {
    self.pageControl.numberOfPages = viewModel.accounts.count;
    self.pageControl.pageIndicatorTintColor = viewModel.pageIndicatorTintColor;
    self.pageControl.currentPageIndicatorTintColor = viewModel.currentPageIndicatorTintColor;
    
    NSInteger selectedAccountIndex = [viewModel.accounts indexOfObject:viewModel.selectedAccount];
    self.pageControl.currentPage = selectedAccountIndex;
    
    [self.collectionViewManager updateWithViewModels:viewModel.accounts
                            selectedAccountViewModel:viewModel.selectedAccount];
}

- (BOOL)doesReceiveInputs {
    return [self.collectionViewManager doesReceiveInputs];
}

#pragma mark - AccountSelectionCollectionViewManagerDelegate

- (void)didSelectCurrency:(KMCurrency)currency {
    self.selectedCurrency = currency;
    
    [self.delegate didSelectCurrencyInAccountSelectionView:self];
}

- (void)didEnterAmount:(NSDecimalNumber *)amount {
    self.inputtedAmount = amount;
    
    [self.delegate didEnterAmountToAccountSelectionView:self];
}

@end
