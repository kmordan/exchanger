//
//  AccountSelectionViewDelegate.h
//  exchanger
//
//  Created by Konstantin Mordan on 19/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AccountSelectionView;

NS_ASSUME_NONNULL_BEGIN

@protocol AccountSelectionViewDelegate <NSObject>

- (void)didSelectCurrencyInAccountSelectionView:(AccountSelectionView *)accountSelectionView;
- (void)didEnterAmountToAccountSelectionView:(AccountSelectionView *)accountSelectionView;

@end

NS_ASSUME_NONNULL_END
