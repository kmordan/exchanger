//
//  AccountSelectionView.h
//  exchanger
//
//  Created by Konstantin Mordan on 12/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AccountSelectionCollectionViewManagerDelegate.h"

@class AccountSelectionCollectionViewManager;
@class AccountSelectionViewModel;

@protocol AccountSelectionViewDelegate;

NS_ASSUME_NONNULL_BEGIN

@interface AccountSelectionView : UIView <AccountSelectionCollectionViewManagerDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) AccountSelectionCollectionViewManager *collectionViewManager;

@property (nonatomic, strong) UIPageControl *pageControl;

@property (nonatomic, assign, readonly) KMCurrency selectedCurrency;
@property (nonatomic, assign, readonly, nullable) NSDecimalNumber *inputtedAmount;

@property (nonatomic, weak) id<AccountSelectionViewDelegate> delegate;

- (void)updateWithViewModel:(AccountSelectionViewModel *)viewModel;

- (BOOL)doesReceiveInputs;

@end

NS_ASSUME_NONNULL_END
