//
//  AccountCollectionViewCell.h
//  exchanger
//
//  Created by Konstantin Mordan on 12/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AccountViewModel;

@protocol AccountCollectionViewCellDelegate;

@interface AccountCollectionViewCell : UICollectionViewCell <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UILabel *currencyNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *availableAmountLabel;
@property (nonatomic, weak) IBOutlet UITextField *enteredAmountTextField;
@property (nonatomic, weak) IBOutlet UILabel *exchangeRateLabel;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

- (void)updateCellWithViewModel:(AccountViewModel *)viewModel
                       delegate:(id<AccountCollectionViewCellDelegate>)delegate;

@end
