//
//  AccountCollectionViewCell.m
//  exchanger
//
//  Created by Konstantin Mordan on 12/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "AccountCollectionViewCell.h"
#import "AccountCollectionViewCellDelegate.h"
#import "AccountViewModel.h"

static NSUInteger const KMPrefixIndex = 1u;

@interface AccountCollectionViewCell ()

@property (nonatomic, copy) NSString *prefix;
@property (nonatomic, weak) id<AccountCollectionViewCellDelegate> delegate;

@end

@implementation AccountCollectionViewCell

#pragma mark - Lifecycle

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.enteredAmountTextField.delegate = self;
}

- (BOOL)becomeFirstResponder {
    return [self.enteredAmountTextField becomeFirstResponder];
}

#pragma mark - Public methods

- (void)updateCellWithViewModel:(AccountViewModel *)viewModel
                       delegate:(id<AccountCollectionViewCellDelegate>)delegate {
    self.currencyNameLabel.text = viewModel.currencyName;
    self.currencyNameLabel.textColor = viewModel.currencyNameTextColor;
    
    self.availableAmountLabel.attributedText = viewModel.availableAmount;
    self.exchangeRateLabel.text = viewModel.rate;
    self.exchangeRateLabel.textColor = viewModel.rateTextColor;
    
    NSString *amount = nil;
    if (viewModel.inputtedAmount != nil) {
        amount = [NSString stringWithFormat:@"%@%@", viewModel.prefix, viewModel.inputtedAmount];
    }
    
    self.enteredAmountTextField.text = amount;
    self.enteredAmountTextField.textColor = viewModel.inputtedAmountTextColor;
    self.enteredAmountTextField.tintColor = viewModel.inputtedAmountTextColor;
    
    self.prefix = viewModel.prefix;
    self.delegate = delegate;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSMutableString *text = [textField.text mutableCopy];
    [text replaceCharactersInRange:range
                        withString:string];
    
    if ([string isEqualToString:[NSLocale currentLocale].decimalSeparator]) {
        textField.text = [text copy];
        return NO;
    }
    
    NSString *amountString = [text copy];
    if ([amountString hasPrefix:self.prefix]) {
        amountString = [amountString substringFromIndex:KMPrefixIndex];
    }
    
    NSDecimalNumber *amount = nil;
    if ([amountString isEqualToString:@""] == NO) {
        amount = [NSDecimalNumber decimalNumberWithString:amountString
                                                   locale:[NSLocale currentLocale]];
    }

    [self.delegate didEnterAmount:amount];

    return NO;
}

@end
