//
//  ExchangeViewController.h
//  exchanger
//
//  Created by Konstantin Mordan on 09/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+KMTransitionHandler.h"
#import "ExchangeViewInput.h"
#import "AccountSelectionViewDelegate.h"

@protocol ExchangeViewOutput;
@protocol LoadingIndicatorManager;

NS_ASSUME_NONNULL_BEGIN

@interface ExchangeViewController : UIViewController <ExchangeViewInput, AccountSelectionViewDelegate>

@property (nonatomic, strong) id<ExchangeViewOutput> output;
@property (nonatomic, strong) id<LoadingIndicatorManager> loadingIndicatorManager;

@property (nonatomic, strong) NSNotificationCenter *notificationCenter;

@end

NS_ASSUME_NONNULL_END
