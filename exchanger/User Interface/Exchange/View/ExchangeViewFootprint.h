//
//  ExchangeViewFootprint.h
//  exchanger
//
//  Created by Konstantin Mordan on 19/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonConstants.h"

NS_ASSUME_NONNULL_BEGIN

/**
 @author Konstantin Mordan
 
 DTO с отпечатком ключевых состояний вьюхи
 */
@interface ExchangeViewFootprint : NSObject

/**
 @author Konstantin Mordan
 
 Тип обмена
 */
@property (nonatomic, assign, readonly) KMExchangingType exchangingType;

/**
 @author Konstantin Mordan
 
 Введанная сумма
 */
@property (nonatomic, strong, readonly) NSDecimalNumber *amount;

/**
 @author Konstantin Mordan
 
 Выбранная валюта из которой будет выполнен обмен
 */
@property (nonatomic, assign, readonly) KMCurrency fromCurrency;

/**
 @author Konstantin Mordan
 
 Выбранная валюта в которую будет выполнен обмен
 */
@property (nonatomic, assign, readonly) KMCurrency toCurrency;

+ (instancetype)footprintWithExchangingType:(KMExchangingType)exchangingType
                                     amount:(NSDecimalNumber *)amount
                               fromCurrency:(KMCurrency)fromCurrency
                                 toCurrency:(KMCurrency)toCurrency;

- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
