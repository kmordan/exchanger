//
//  ExchangeViewModel.h
//  exchanger
//
//  Created by Konstantin Mordan on 20/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AccountSelectionViewModel;

NS_ASSUME_NONNULL_BEGIN

@interface ExchangeViewModel : NSObject

@property (nonatomic, strong, readonly) UIColor *titleColor;
@property (nonatomic, strong, readonly) UIColor *exchangeButtonColor;

@property (nonatomic, copy) NSString *rate;
@property (nonatomic, assign, getter=isExchangeAvailable) BOOL exchangeAvailable;
@property (nonatomic, strong) AccountSelectionViewModel *accountSelectionViewModelForExchangingFrom;
@property (nonatomic, strong) AccountSelectionViewModel *accountSelectionViewModelForExchangingTo;


@end

NS_ASSUME_NONNULL_END
