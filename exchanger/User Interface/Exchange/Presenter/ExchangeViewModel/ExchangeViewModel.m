//
//  ExchangeViewModel.m
//  exchanger
//
//  Created by Konstantin Mordan on 20/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "ExchangeViewModel.h"
#import "UIColor+KMPalette.h"

@implementation ExchangeViewModel

- (UIColor *)titleColor {
    return [UIColor km_primaryColor];
}

- (UIColor *)exchangeButtonColor {
    return [UIColor km_primaryColor];
}

@end
