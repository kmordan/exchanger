//
//  AccountSelectionViewModel.h
//  exchanger
//
//  Created by Konstantin Mordan on 20/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AccountViewModel;

NS_ASSUME_NONNULL_BEGIN

@interface AccountSelectionViewModel : NSObject

@property (nonatomic, strong, readonly) UIColor *pageIndicatorTintColor;
@property (nonatomic, strong, readonly) UIColor *currentPageIndicatorTintColor;

@property (nonatomic, copy) NSArray<AccountViewModel *> *accounts;
@property (nonatomic, strong) AccountViewModel *selectedAccount;

@end

NS_ASSUME_NONNULL_END
