//
//  AccountSelectionViewModel.m
//  exchanger
//
//  Created by Konstantin Mordan on 20/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "AccountSelectionViewModel.h"
#import "UIColor+KMPalette.h"

@implementation AccountSelectionViewModel

- (UIColor *)pageIndicatorTintColor {
    return [UIColor km_thirdColor];
}

- (UIColor *)currentPageIndicatorTintColor {
    return [UIColor km_primaryColor];
}

@end
