//
//  AccountViewModel.m
//  exchanger
//
//  Created by Konstantin Mordan on 20/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "AccountViewModel.h"
#import "UIColor+KMPalette.h"

@implementation AccountViewModel

- (UIColor *)currencyNameTextColor {
    return [UIColor km_primaryColor];
}

- (UIColor *)availableAmountTextColor {
    return [UIColor km_secondaryColor];
}

- (UIColor *)inputtedAmountTextColor {
    return [UIColor km_primaryColor];
}

- (UIColor *)rateTextColor {
    return [UIColor km_secondaryColor];
}

@end
