//
//  AccountViewModel.h
//  exchanger
//
//  Created by Konstantin Mordan on 20/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonConstants.h"

NS_ASSUME_NONNULL_BEGIN

@interface AccountViewModel : NSObject

@property (nonatomic, strong, readonly) UIColor *currencyNameTextColor;
@property (nonatomic, strong, readonly) UIColor *availableAmountTextColor;
@property (nonatomic, strong, readonly) UIColor *inputtedAmountTextColor;
@property (nonatomic, strong, readonly) UIColor *rateTextColor;

@property (nonatomic, assign) KMCurrency currency;

@property (nonatomic, copy) NSString *currencyName;
@property (nonatomic, copy) NSString *currencySign;
@property (nonatomic, copy, nullable) NSString *inputtedAmount;
@property (nonatomic, copy) NSAttributedString *availableAmount;
@property (nonatomic, copy, nullable) NSString *rate;
@property (nonatomic, copy) NSString *prefix;
@property (nonatomic, assign, getter=isFirstResponder) BOOL firstResponder;

@end

NS_ASSUME_NONNULL_END
