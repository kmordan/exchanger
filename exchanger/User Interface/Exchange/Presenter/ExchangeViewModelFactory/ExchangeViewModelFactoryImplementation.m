//
//  ExchangeViewModelFactoryImplementation.m
//  exchanger
//
//  Created by Konstantin Mordan on 20/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "ExchangeViewModelFactoryImplementation.h"
#import "Account.h"
#import "ExchangeRatesAttendant.h"

#import "AccountViewModel.h"
#import "AccountSelectionViewModel.h"
#import "ExchangeViewModel.h"

#import "ExchangeViewFootprint.h"

#import "UIColor+KMPalette.h"

static NSString * const KMRateStringFormat = @"%@1 = %@%@";
static NSString * const KMAvailableAccountBalanceFormat = @"You have %@%@";

typedef NS_ENUM(NSUInteger, KMMaximumFractionDigits) {
    KMMaximumFractionDigits0 = 0u,
    KMMaximumFractionDigits2 = 2u,
    KMMaximumFractionDigits4 = 4u
};

@interface ExchangeViewModelFactoryImplementation ()

@property (nonatomic, copy) NSDictionary<NSNumber *, NSString *> *currencySigns;
@property (nonatomic, copy) NSDictionary<NSNumber *, NSString *> *currencyNames;
@property (nonatomic, copy) NSDictionary<NSNumber *, NSString *> *prefixes;

@end

@implementation ExchangeViewModelFactoryImplementation

#pragma mark - Public methods

- (ExchangeViewModel *)exchangeViewModelWithAccounts:(NSArray<Account *> *)accounts
                              exchangeRatesAttendant:(ExchangeRatesAttendant *)exchangeRatesAttendant
                                       viewFootprint:(ExchangeViewFootprint *)viewFootprint
                                      exchangeResult:(NSDecimalNumber *)exchangeResult {
    NSArray<AccountViewModel *> *fromAccountsViewModels = [self accountViewModelsForExchangingType:KMExchangingTypeFrom
                                                                                      withAccounts:accounts
                                                                            exchangeRatesAttendant:exchangeRatesAttendant
                                                                                     viewFootprint:viewFootprint
                                                                                    exchangeResult:exchangeResult];
    NSArray<AccountViewModel *> *toAccountsViewModels = [self accountViewModelsForExchangingType:KMExchangingTypeTo
                                                                                    withAccounts:accounts
                                                                          exchangeRatesAttendant:exchangeRatesAttendant
                                                                                   viewFootprint:viewFootprint
                                                                                  exchangeResult:exchangeResult];
    
    AccountSelectionViewModel *accountSelectionViewModelForExchangingFrom = [self accountSelectionViewModelWithAccountsViewModels:fromAccountsViewModels
                                                                                                                 selectedCurrency:viewFootprint.fromCurrency];
    AccountSelectionViewModel *accountSelectionViewModelForExchangingTo = [self accountSelectionViewModelWithAccountsViewModels:toAccountsViewModels
                                                                                                               selectedCurrency:viewFootprint.toCurrency];
    
    NSString *rate = [self rateStringFromCurrency:viewFootprint.fromCurrency
                                       toCurrency:viewFootprint.toCurrency
                       withExchangeRatesAttendant:exchangeRatesAttendant];
    
    Account *accountForExchangingFrom = nil;
    for (Account *account in accounts) {
        if (account.currency == viewFootprint.fromCurrency) {
            accountForExchangingFrom = account;
            break;
        }
    }
    
    NSDecimalNumber *availableAccountBalance = accountForExchangingFrom.balance;
    NSDecimalNumber *requiredAccountBalance = viewFootprint.exchangingType == KMExchangingTypeFrom ? viewFootprint.amount : exchangeResult;
    
    BOOL exchangeAvailable = NO;
    if (requiredAccountBalance != nil && accountSelectionViewModelForExchangingFrom.selectedAccount.currency != accountSelectionViewModelForExchangingTo.selectedAccount.currency) {
        NSComparisonResult result = [availableAccountBalance compare:requiredAccountBalance];
        exchangeAvailable = (result == NSOrderedDescending || result == NSOrderedSame);
    }
    
    ExchangeViewModel *viewModel = [ExchangeViewModel new];
    viewModel.rate = rate;
    viewModel.exchangeAvailable = exchangeAvailable;
    viewModel.accountSelectionViewModelForExchangingFrom = accountSelectionViewModelForExchangingFrom;
    viewModel.accountSelectionViewModelForExchangingTo = accountSelectionViewModelForExchangingTo;
    
    return viewModel;
}

- (ExchangeViewModel *)exchangeViewModelWithAccounts:(NSArray<Account *> *)accounts {
    NSArray<AccountViewModel *> *fromAccountsViewModels = [self accountViewModelsForExchangingType:KMExchangingTypeFrom
                                                                                      withAccounts:accounts
                                                                            exchangeRatesAttendant:nil
                                                                                     viewFootprint:nil
                                                                                    exchangeResult:nil];
    NSArray<AccountViewModel *> *toAccountsViewModels = [self accountViewModelsForExchangingType:KMExchangingTypeTo
                                                                                    withAccounts:accounts
                                                                          exchangeRatesAttendant:nil
                                                                                   viewFootprint:nil
                                                                                  exchangeResult:nil];
    
    AccountSelectionViewModel *accountSelectionViewModelForExchangingFrom = [self accountSelectionViewModelWithAccountsViewModels:fromAccountsViewModels
                                                                                                                 selectedCurrency:accounts.firstObject.currency];
    AccountSelectionViewModel *accountSelectionViewModelForExchangingTo = [self accountSelectionViewModelWithAccountsViewModels:toAccountsViewModels
                                                                                                               selectedCurrency:accounts.firstObject.currency];
    
    ExchangeViewModel *viewModel = [ExchangeViewModel new];
    viewModel.exchangeAvailable = NO;
    viewModel.accountSelectionViewModelForExchangingFrom = accountSelectionViewModelForExchangingFrom;
    viewModel.accountSelectionViewModelForExchangingTo = accountSelectionViewModelForExchangingTo;
    
    return viewModel;
}

#pragma mark - Helper methods

- (NSArray<AccountViewModel *> *)accountViewModelsForExchangingType:(KMExchangingType)exchangingType
                                                       withAccounts:(NSArray<Account *> *)accounts
                                             exchangeRatesAttendant:(ExchangeRatesAttendant *)exchangeRatesAttendant
                                                      viewFootprint:(ExchangeViewFootprint *)viewFootprint
                                                     exchangeResult:(NSDecimalNumber *)exchangeResult {
    NSMutableArray<AccountViewModel *> *accountViewModels = [NSMutableArray new];
    
    for (Account *account in accounts) {
        KMCurrency currency = account.currency;
        NSString *currencyName = self.currencyNames[@(currency)];
        NSString *currencySign = self.currencySigns[@(currency)];
        
        NSDecimalNumber *amount = viewFootprint.exchangingType == exchangingType ? viewFootprint.amount : exchangeResult;
        
        KMMaximumFractionDigits inputtedAmountMaximumFractionDigits = [self maximumFractionDigitsForAmount:amount];
        NSString *inputtedAmount = [self stringFromNumber:amount
                                    maximumFractionDigits:inputtedAmountMaximumFractionDigits];
        
        KMMaximumFractionDigits availableAmountMaximumFractionDigits = [self maximumFractionDigitsForAmount:account.balance];
        NSString *availableAmountString = [NSString stringWithFormat:KMAvailableAccountBalanceFormat, currencySign, [self stringFromNumber:account.balance
                                                                                                                     maximumFractionDigits:availableAmountMaximumFractionDigits]];

        NSAttributedString *availableAmount = nil;
        
        if (exchangingType == KMExchangingTypeFrom && amount != nil) {
            NSComparisonResult result = [account.balance compare:amount];

            UIColor *textColor = (result == NSOrderedDescending || result == NSOrderedSame) ? [UIColor km_secondaryColor] : [UIColor km_accentColor];
            NSDictionary<NSAttributedStringKey, id> *attributes = @{ NSForegroundColorAttributeName : textColor };
            availableAmount = [[NSAttributedString alloc] initWithString:availableAmountString
                                                              attributes:attributes];
        } else {
            availableAmount = [[NSAttributedString alloc] initWithString:availableAmountString];
        }
        
        NSString *rate = exchangingType == KMExchangingTypeFrom ? nil : [self rateStringFromCurrency:viewFootprint.fromCurrency
                                                                                          toCurrency:viewFootprint.toCurrency
                                                                          withExchangeRatesAttendant:exchangeRatesAttendant];
        
        NSString *prefix = self.prefixes[@(exchangingType)];
        BOOL isFirstResponder = viewFootprint.exchangingType == exchangingType;
        
        AccountViewModel *viewModel = [AccountViewModel new];
        viewModel.currency = currency;
        viewModel.currencyName = currencyName;
        viewModel.currencySign = currencySign;
        viewModel.availableAmount = availableAmount;
        viewModel.inputtedAmount = inputtedAmount;
        viewModel.rate = rate;
        viewModel.prefix = prefix;
        viewModel.firstResponder = isFirstResponder;
        
        [accountViewModels addObject:viewModel];
    }

    return [accountViewModels copy];
}

- (AccountSelectionViewModel *)accountSelectionViewModelWithAccountsViewModels:(NSArray<AccountViewModel *> *)accountsViewModels
                                                              selectedCurrency:(KMCurrency)selectedCurrency {
    AccountViewModel *selectedAccountViewModel = nil;
    
    for (AccountViewModel *viewModel in accountsViewModels) {
        if (viewModel.currency == selectedCurrency) {
            selectedAccountViewModel = viewModel;
            break;
        }
    }
    
    AccountSelectionViewModel *viewModel = [AccountSelectionViewModel new];
    viewModel.accounts = accountsViewModels;
    viewModel.selectedAccount = selectedAccountViewModel;
    
    return viewModel;
}

- (NSString *)rateStringFromCurrency:(KMCurrency)fromCurrency
                          toCurrency:(KMCurrency)toCurrency
          withExchangeRatesAttendant:(ExchangeRatesAttendant *)exchangeRatesAttendant {
    NSString *fromCurrencySign = self.currencySigns[@(fromCurrency)];
    NSString *toCurrencySign = self.currencySigns[@(toCurrency)];

    NSDecimalNumber *rate = [exchangeRatesAttendant exchangeRateFromCurrency:fromCurrency
                                                                  toCurrency:toCurrency];
    
    if (rate == nil) {
        return @"";
    }

    return [NSString stringWithFormat:KMRateStringFormat, fromCurrencySign, toCurrencySign, [self stringFromNumber:rate
                                                                                             maximumFractionDigits:KMMaximumFractionDigits4]];
}

- (KMMaximumFractionDigits)maximumFractionDigitsForAmount:(NSDecimalNumber *)amount {
    NSDecimalNumberHandler *handler = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain
                                                                                             scale:0
                                                                                  raiseOnExactness:YES
                                                                                   raiseOnOverflow:YES
                                                                                  raiseOnUnderflow:YES
                                                                               raiseOnDivideByZero:YES];
    NSDecimalNumber *roundedAmount = [amount decimalNumberByRoundingAccordingToBehavior:handler];
    
    return [amount compare:roundedAmount] == NSOrderedSame ? KMMaximumFractionDigits0 : KMMaximumFractionDigits2;
}

- (NSString *)stringFromNumber:(NSDecimalNumber *)number
         maximumFractionDigits:(KMMaximumFractionDigits)maximumFractionDigits {
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    formatter.currencySymbol = @"";
    formatter.positiveSuffix = @"";
    formatter.negativeSuffix = @"";
    formatter.groupingSeparator = @"";
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    formatter.maximumFractionDigits = maximumFractionDigits;

    return [formatter stringFromNumber:number];
}

#pragma mark - Lazy properties

- (NSDictionary<NSNumber *, NSString *> *)currencySigns {
    if (_currencySigns == nil) {
        _currencySigns = @{ @(KMCurrencyEUR) : @"€",
                            @(KMCurrencyUSD) : @"$",
                            @(KMCurrencyGBP) : @"£" };
    }
    
    return _currencySigns;
}

- (NSDictionary<NSNumber *, NSString *> *)currencyNames {
    if (_currencyNames == nil) {
        _currencyNames = @{ @(KMCurrencyEUR) : @"EUR",
                            @(KMCurrencyUSD) : @"USD",
                            @(KMCurrencyGBP) : @"GBP" };
    }
    
    return _currencyNames;
}

- (NSDictionary<NSNumber *, NSString *> *)prefixes {
    if (_prefixes == nil) {
        _prefixes = @{ @(KMExchangingTypeFrom) : @"-",
                       @(KMExchangingTypeTo)   : @"+" };
    }
    
    return _prefixes;
}

@end
