//
//  ExchangeViewModelFactoryImplementation.h
//  exchanger
//
//  Created by Konstantin Mordan on 20/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExchangeViewModelFactory.h"

/**
 @author Konstantin Mordan
 
 Реализация фабрики создания вью-модели для ExchangeViewController
 */
@interface ExchangeViewModelFactoryImplementation : NSObject <ExchangeViewModelFactory>

@end
