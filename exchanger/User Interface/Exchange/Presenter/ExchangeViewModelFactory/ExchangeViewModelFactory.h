//
//  ExchangeViewModelFactory.h
//  exchanger
//
//  Created by Konstantin Mordan on 20/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ExchangeViewModel;
@class ExchangeRatesAttendant;
@class Account;
@class ExchangeViewFootprint;

/**
 @author Konstantin Mordan
 
 Протокол фабрики создания вью-модели для ExchangeViewController
 */
@protocol ExchangeViewModelFactory <NSObject>

/**
 @author Konstantin Mordan
 
 Метод создания вью-модель ExchangeViewController`а

 @param accounts               Массив счетов
 @param exchangeRatesAttendant Объект, обслуживающий курсы валют
 @param viewFootprint          Отпечаток вью
 @param exchangeResult         Результат обмена валют
 */
- (ExchangeViewModel *)exchangeViewModelWithAccounts:(NSArray<Account *> *)accounts
                              exchangeRatesAttendant:(ExchangeRatesAttendant *)exchangeRatesAttendant
                                       viewFootprint:(ExchangeViewFootprint *)viewFootprint
                                      exchangeResult:(NSDecimalNumber *)exchangeResult;

/**
 @author Konstantin Mordan
 
 Метод создания вью-модель ExchangeViewController`а
 
 @param accounts Массив счетов
 */
- (ExchangeViewModel *)exchangeViewModelWithAccounts:(NSArray<Account *> *)accounts;

@end
