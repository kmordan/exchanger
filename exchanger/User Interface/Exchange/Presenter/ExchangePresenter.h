//
//  ExchangePresenter.h
//  exchanger
//
//  Created by Konstantin Mordan on 09/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ExchangeViewOutput.h"
#import "ExchangeInteractorOutput.h"

@protocol ExchangeViewInput;
@protocol ExchangeInteractorInput;
@protocol ExchangeRouterInput;

@protocol Exchanger;
@protocol ExchangeViewModelFactory;

NS_ASSUME_NONNULL_BEGIN

@interface ExchangePresenter : NSObject <ExchangeViewOutput, ExchangeInteractorOutput>

@property (nonatomic, weak) id<ExchangeViewInput> view;
@property (nonatomic, strong) id<ExchangeInteractorInput> interactor;
@property (nonatomic, strong) id<ExchangeRouterInput> router;

@property (nonatomic, strong) id<Exchanger> exchanger;
@property (nonatomic, strong) id<ExchangeViewModelFactory> viewModelFactory;

@end

NS_ASSUME_NONNULL_END
