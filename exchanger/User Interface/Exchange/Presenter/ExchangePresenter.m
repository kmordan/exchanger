//
//  ExchangePresenter.m
//  exchanger
//
//  Created by Konstantin Mordan on 09/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "ExchangePresenter.h"
#import "ExchangeViewInput.h"
#import "ExchangeInteractorInput.h"
#import "ExchangeViewFootprint.h"
#import "Exchanger.h"
#import "ExchangeViewModelFactory.h"

#import "ExchangeRatesAttendant.h"
#import "ExchangeTransaction.h"

@interface ExchangePresenter ()

@property (nonatomic, copy) NSArray<Account *> *accounts;
@property (nonatomic, strong) ExchangeRatesAttendant *exchangeRatesAttendant;
@property (nonatomic, strong) ExchangeViewFootprint *viewFootprint;

@end

@implementation ExchangePresenter

#pragma mark - ExchangeViewOutput

- (void)didLoadView {
    [self.view showLoadingIndicator];
    
    [self.interactor obtainAccounts];
    [self.interactor beginLoadingExchangeRates];
}

- (void)didUpdateFootprint:(ExchangeViewFootprint *)newFootprint {
    self.viewFootprint = newFootprint;
    
    NSDecimalNumber *exchangeResult = [self.exchanger exchangeAmount:self.viewFootprint.amount
                                                        fromCurrency:self.viewFootprint.fromCurrency
                                                          toCurrency:self.viewFootprint.toCurrency
                                          withExchangeRatesAttendant:self.exchangeRatesAttendant];
    
    ExchangeViewModel *viewModel = [self.viewModelFactory exchangeViewModelWithAccounts:self.accounts
                                                                 exchangeRatesAttendant:self.exchangeRatesAttendant
                                                                          viewFootprint:self.viewFootprint
                                                                         exchangeResult:exchangeResult];
    
    [self.view updateWithViewModel:viewModel];
}

- (void)didInitiateExchangeWithFootprint:(ExchangeViewFootprint *)footprint {
    self.viewFootprint = footprint;

    KMCurrency fromCurrency = footprint.fromCurrency;
    KMCurrency toCurrency = footprint.toCurrency;
    NSDecimalNumber *amount = footprint.amount;
    
    NSDecimalNumber *exchangeResult = [self.exchanger exchangeAmount:self.viewFootprint.amount
                                                        fromCurrency:self.viewFootprint.fromCurrency
                                                          toCurrency:self.viewFootprint.toCurrency
                                          withExchangeRatesAttendant:self.exchangeRatesAttendant];
    
    ExchangeTransaction *exchangeTransaction = [ExchangeTransaction transactionFromCurrency:fromCurrency
                                                                                 toCurrency:toCurrency
                                                                              expenseAmount:amount
                                                                               incomeAmount:exchangeResult];
    
    [self.interactor applyExchangeTransaction:exchangeTransaction];
}

#pragma mark - ExchangeInteractorOutput

- (void)didLoadExchangeRates:(ExchangeRates *)exchangeRates {
    self.exchangeRatesAttendant = [ExchangeRatesAttendant attendantWithExchangeRates:exchangeRates];
    
    if (self.accounts != nil) {
        ExchangeViewModel *viewModel = [self generateExchangeViewModel];
        
        [self.view updateWithViewModel:viewModel];
    }
}

- (void)didObtainAccounts:(NSArray<Account *> *)accounts {
    self.accounts = accounts;
    
    ExchangeViewModel *viewModel = nil;
    if (self.exchangeRatesAttendant != nil) {
        viewModel = [self generateExchangeViewModel];
        
    } else {
        viewModel = [self.viewModelFactory exchangeViewModelWithAccounts:self.accounts];
    }

    [self.view updateWithViewModel:viewModel];
    
    [self.view hideLoadingIndicator];
}

#pragma maek - Helper methods

- (ExchangeViewModel *)generateExchangeViewModel {
    NSDecimalNumber *exchangeResult = [self.exchanger exchangeAmount:self.viewFootprint.amount
                                                        fromCurrency:self.viewFootprint.fromCurrency
                                                          toCurrency:self.viewFootprint.toCurrency
                                          withExchangeRatesAttendant:self.exchangeRatesAttendant];
    
    ExchangeViewModel *viewModel = [self.viewModelFactory exchangeViewModelWithAccounts:self.accounts
                                                                 exchangeRatesAttendant:self.exchangeRatesAttendant
                                                                          viewFootprint:self.viewFootprint
                                                                         exchangeResult:exchangeResult];
    return viewModel;
}

@end
