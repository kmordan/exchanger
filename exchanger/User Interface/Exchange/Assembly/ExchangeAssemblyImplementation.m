//
//  ExchangeAssemblyImplementation.m
//  exchanger
//
//  Created by Konstantin Mordan on 10/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "ExchangeAssemblyImplementation.h"
#import "UserInterfaceConstants.h"

#import "ExchangeViewController.h"
#import "ExchangePresenter.h"
#import "ExchangeInteractor.h"
#import "ExchangeRouter.h"

#import "OriginalLoadingIndicatorManager.h"
#import "OriginalExchanger.h"
#import "ExchangeViewModelFactoryImplementation.h"
#import "ExchangeRatesServiceImplementation.h"
#import "AccountsStorageImplementation.h"

@implementation ExchangeAssemblyImplementation

- (UIViewController *)exchangeViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:KMMainStoryboardName
                                                         bundle:[NSBundle bundleForClass:[self class]]];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:KMExchangeViewControllerIdentifier];
    
    if ([vc isKindOfClass:[ExchangeViewController class]]) {
        ExchangeViewController *exchangeViewController = (ExchangeViewController *)vc;
        
        OriginalLoadingIndicatorManager *loadingIndicatorManager = [OriginalLoadingIndicatorManager new];
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        OriginalExchanger *exchanger = [OriginalExchanger new];
        ExchangeViewModelFactoryImplementation *viewModelFactory = [ExchangeViewModelFactoryImplementation new];
        
        ExchangeRatesServiceImplementation *exchangeRatesService = [ExchangeRatesServiceImplementation new];
        exchangeRatesService.queue = [NSOperationQueue new];
        
        AccountsStorageImplementation *accountsStorage = [AccountsStorageImplementation new];
        
        ExchangePresenter *presenter = [ExchangePresenter new];
        ExchangeInteractor *interactor = [ExchangeInteractor new];
        ExchangeRouter *router = [ExchangeRouter new];
        
        exchangeViewController.output = presenter;
        exchangeViewController.loadingIndicatorManager = loadingIndicatorManager;
        exchangeViewController.notificationCenter = notificationCenter;
        
        presenter.view = exchangeViewController;
        presenter.interactor = interactor;
        presenter.router = router;
        presenter.exchanger = exchanger;
        presenter.viewModelFactory = viewModelFactory;
        
        interactor.output = presenter;
        interactor.exchangeRatesService = exchangeRatesService;
        interactor.accountsStorage = accountsStorage;
        
        router.transitionHandler = exchangeViewController;
    }
    
    return vc;
}

@end
