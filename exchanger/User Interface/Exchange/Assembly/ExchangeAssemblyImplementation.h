//
//  ExchangeAssemblyImplementation.h
//  exchanger
//
//  Created by Konstantin Mordan on 10/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ExchangeAssembly.h"

/**
 @author Konstantin Mordan
 
 Реализация ассембли модуля Exchange
 */
@interface ExchangeAssemblyImplementation : NSObject <ExchangeAssembly>

@end
