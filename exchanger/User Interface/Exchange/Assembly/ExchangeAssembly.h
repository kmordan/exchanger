//
//  ExchangeAssembly.h
//  exchanger
//
//  Created by Konstantin Mordan on 10/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 @author Konstantin Mordan
 
 Ассембли модуля Exchange
 */
@protocol ExchangeAssembly <NSObject>

- (UIViewController *)exchangeViewController;

@end

NS_ASSUME_NONNULL_END
