//
//  ExchangeInteractorOutput.h
//  exchanger
//
//  Created by Konstantin Mordan on 09/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonConstants.h"

@class Account;

NS_ASSUME_NONNULL_BEGIN

@protocol ExchangeInteractorOutput <NSObject>

/**
 @author Konstantin Mordan
 
 Метод сообщает презентеру о том, что были загружены курсы валют

 @param exchangeRates Загруженные курсы валют
 */
- (void)didLoadExchangeRates:(ExchangeRates *)exchangeRates;

/**
 @author Konstantin Mordan
 
 Метод сообщает презентеру о том, что были были получены счета
 
 @param accounts Полученные счета
 */
- (void)didObtainAccounts:(NSArray<Account *> *)accounts;

@end

NS_ASSUME_NONNULL_END
