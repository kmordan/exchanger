//
//  ExchangeInteractor.h
//  exchanger
//
//  Created by Konstantin Mordan on 09/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ExchangeInteractorInput.h"

@protocol ExchangeInteractorOutput;
@protocol ExchangeRatesService;
@protocol AccountsStorage;

NS_ASSUME_NONNULL_BEGIN

@interface ExchangeInteractor : NSObject <ExchangeInteractorInput>

@property (nonatomic, weak) id<ExchangeInteractorOutput> output;

@property (nonatomic, strong) id<ExchangeRatesService> exchangeRatesService;
@property (nonatomic, strong) id<AccountsStorage> accountsStorage;

@end

NS_ASSUME_NONNULL_END
