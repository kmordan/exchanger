//
//  ExchangeInteractor.m
//  exchanger
//
//  Created by Konstantin Mordan on 09/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "ExchangeInteractor.h"
#import "ExchangeInteractorOutput.h"
#import "Account.h"
#import "ExchangeRatesService.h"
#import "AccountsStorage.h"
#import "ExchangeTransaction.h"

static NSTimeInterval const KMExchangeRatesUpdateTimeInterval = 30.0;

@interface ExchangeInteractor ()

@property (nonatomic, weak) NSTimer *timer;

@end

@implementation ExchangeInteractor

#pragma mark - Lifecycle

- (void)dealloc {
    [self.timer invalidate];
    self.timer = nil;
}

#pragma mark - ExchangeInteractorInput

- (void)beginLoadingExchangeRates {
    [self loadExchangeRates];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:KMExchangeRatesUpdateTimeInterval
                                                  target:self
                                                selector:@selector(loadExchangeRates)
                                                userInfo:nil
                                                 repeats:YES];
}

- (void)stopLoadingExchangeRates {
    [self.timer invalidate];
    self.timer = nil;
    
    [self.exchangeRatesService cancelObtainingExchangeRates];
}

- (void)obtainAccounts {
    NSArray<Account *> *accounts = [self.accountsStorage obtainAccounts];
    
    [self.output didObtainAccounts:accounts];
}

- (void)applyExchangeTransaction:(ExchangeTransaction *)exchangeTransaction {
    KMCurrency fromCurrency = exchangeTransaction.fromCurrency;
    KMCurrency toCurrency = exchangeTransaction.toCurrency;
    
    if (fromCurrency == toCurrency) {
        return;
    }
    
    NSDecimalNumber *expenseAmount = exchangeTransaction.expenseAmount;
    NSDecimalNumber *incomeAmount = exchangeTransaction.incomeAmount;
    
    NSArray<Account *> *accountsBeforeExchange = [self.accountsStorage obtainAccounts];
    
    NSMutableArray<Account *> *accountsAfterExchange = [accountsBeforeExchange mutableCopy];

    for (Account *account in accountsBeforeExchange) {
        Account *newAccount = nil;
        
        if (account.currency == fromCurrency) {
            NSDecimalNumber *newAmount = [account.balance decimalNumberBySubtracting:expenseAmount];
            newAccount = [Account accountWithBalance:newAmount
                                        ofCurrency:account.currency];
        }
        
        if (account.currency == toCurrency) {
            NSDecimalNumber *newAmount = [account.balance decimalNumberByAdding:incomeAmount];
            newAccount = [Account accountWithBalance:newAmount
                                        ofCurrency:account.currency];
        }
        
        if (newAccount != nil) {
            NSUInteger index = [accountsAfterExchange indexOfObject:account];
            [accountsAfterExchange replaceObjectAtIndex:index
                                             withObject:newAccount];
        }
    }
    __weak typeof(self) wSelf = self;
    [self.accountsStorage saveAccounts:[accountsAfterExchange copy]
                         withComletion:^(NSArray<Account *> *savedAccounts, BOOL success) {
                             typeof(self) sSelf = wSelf;
                             if (success) {
                                 [sSelf.output didObtainAccounts:savedAccounts];
                             }
                         }];
}

#pragma mark - Internal methods

- (void)loadExchangeRates {
    __weak typeof(self) wSelf = self;
    [self.exchangeRatesService obtainExchangeRatesWithCompletion:^(ExchangeRates * _Nullable exchangeRates, NSError * _Nullable error)
     {
         __strong typeof(self) sSelf = wSelf;
         if (exchangeRates != nil) {
             [sSelf.output didLoadExchangeRates:exchangeRates];
         }
     }];
}

@end
