//
//  ExchangeInteractorInput.h
//  exchanger
//
//  Created by Konstantin Mordan on 09/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ExchangeTransaction;

@protocol ExchangeInteractorInput <NSObject>

/**
 @author Konstantin Mordan
 
 Метод инициирует зугрузку курсов валют
 */
- (void)beginLoadingExchangeRates;

/**
 @author Konstantin Mordan
 
 Метод останавливает зугрузку курсов валют
 */
- (void)stopLoadingExchangeRates;

/**
 @author Konstantin Mordan
 
 Метод инициирует получения счетов
 */
- (void)obtainAccounts;

/**
 @author Konstantin Mordan
 
 Метод применение транзакции обмена к существующий счетам

 @param exchangeTransaction Транзакция обмена
 */
- (void)applyExchangeTransaction:(ExchangeTransaction *)exchangeTransaction;

@end
