//
//  ExchangeRouter.h
//  exchanger
//
//  Created by Konstantin Mordan on 09/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ExchangeRouterInput.h"

@protocol TransitionHandler;

NS_ASSUME_NONNULL_BEGIN

@interface ExchangeRouter : NSObject <ExchangeRouterInput>

@property (nonatomic, weak) id<TransitionHandler> transitionHandler;

@end

NS_ASSUME_NONNULL_END
