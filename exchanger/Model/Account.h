//
//  Account.h
//  exchanger
//
//  Created by Konstantin Mordan on 11/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonConstants.h"

NS_ASSUME_NONNULL_BEGIN

/**
 @author Konstantin Mordan
 
 Счет
 */
@interface Account : NSObject

/**
 @author Konstantin Mordan
 
 Баланс счета
 */
@property (nonatomic, strong, readonly) NSDecimalNumber *balance;

/**
 @author Konstantin Mordan
 
 Валюта счета
 */
@property (nonatomic, assign, readonly) KMCurrency currency;

+ (instancetype)accountWithBalance:(NSDecimalNumber *)balance
                        ofCurrency:(KMCurrency)currency;

- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
