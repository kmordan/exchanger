//
//  ExchangeTransaction.h
//  exchanger
//
//  Created by Konstantin Mordan on 22/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonConstants.h"

NS_ASSUME_NONNULL_BEGIN

/**
 @author Konstantin Mordan
 
 Транзакция обмена валют
 */
@interface ExchangeTransaction : NSObject

/**
 @author Konstantin Mordan
 
 Валюта из которой был выполнен обмен
 */
@property (nonatomic, assign, readonly) KMCurrency fromCurrency;

/**
 @author Konstantin Mordan
 
 Валюта в которую был выполнен обмен
 */
@property (nonatomic, assign, readonly) KMCurrency toCurrency;

/**
 @author Konstantin Mordan
 
 Сумма до обмена
 */
@property (nonatomic, strong, readonly) NSDecimalNumber *expenseAmount;

/**
 @author Konstantin Mordan
 
 Сумма после обмена
 */
@property (nonatomic, strong, readonly) NSDecimalNumber *incomeAmount;

+ (instancetype)transactionFromCurrency:(KMCurrency)fromCurrency
                             toCurrency:(KMCurrency)toCurrency
                          expenseAmount:(NSDecimalNumber *)expenseAmount
                           incomeAmount:(NSDecimalNumber *)incomeAmount;

- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
