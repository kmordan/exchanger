//
//  Account.m
//  exchanger
//
//  Created by Konstantin Mordan on 11/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "Account.h"

@interface Account ()

@property (nonatomic, strong, readwrite) NSDecimalNumber *balance;
@property (nonatomic, assign, readwrite) KMCurrency currency;

@end

@implementation Account

#pragma mark - Initializers

+ (instancetype)accountWithBalance:(NSDecimalNumber *)balance
                        ofCurrency:(KMCurrency)currency {
    return [[self alloc] initWithBalance:balance
                              ofCurrency:currency];
}

-  (instancetype)initWithBalance:(NSDecimalNumber *)balance
                      ofCurrency:(KMCurrency)currency {
    self = [super init];
    
    if (self != nil) {
        _balance = balance;
        _currency = currency;
    }
    
    return self;
}

@end
