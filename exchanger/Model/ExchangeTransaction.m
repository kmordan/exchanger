//
//  ExchangeTransaction.m
//  exchanger
//
//  Created by Konstantin Mordan on 22/10/2017.
//  Copyright © 2017 Konstantin Mordan. All rights reserved.
//

#import "ExchangeTransaction.h"

@interface ExchangeTransaction ()

@property (nonatomic, assign, readwrite) KMCurrency fromCurrency;
@property (nonatomic, assign, readwrite) KMCurrency toCurrency;

@property (nonatomic, strong, readwrite) NSDecimalNumber *expenseAmount;
@property (nonatomic, strong, readwrite) NSDecimalNumber *incomeAmount;

@end

@implementation ExchangeTransaction

+ (instancetype)transactionFromCurrency:(KMCurrency)fromCurrency
                             toCurrency:(KMCurrency)toCurrency
                          expenseAmount:(NSDecimalNumber *)expenseAmount
                           incomeAmount:(NSDecimalNumber *)incomeAmount {
    return [[self alloc] initWithFromCurrency:fromCurrency
                                   toCurrency:toCurrency
                                expenseAmount:expenseAmount
                                 incomeAmount:incomeAmount];
}

- (instancetype)initWithFromCurrency:(KMCurrency)fromCurrency
                          toCurrency:(KMCurrency)toCurrency
                       expenseAmount:(NSDecimalNumber *)expenseAmount
                        incomeAmount:(NSDecimalNumber *)incomeAmount {
    self = [super init];
    
    if (self != nil) {
        _fromCurrency = fromCurrency;
        _toCurrency = toCurrency;
        _expenseAmount = expenseAmount;
        _incomeAmount = incomeAmount;
    }
    
    return self;
}

@end
